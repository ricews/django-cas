"""CAS authentication backend"""

from urllib import urlencode, urlopen
from urlparse import urljoin

from django.conf import settings
import ldap

from ws.django_cas.models import User
from ws.django_cas.signals import cas_user_logged_in

__all__ = ['CASBackend']


def create_cas_connection(
    host_name=getattr(settings, 'LDAP_SERVER', 'ldaps://ldap.rice.edu:636'),
    user=settings.LDAP_USER,
    password=settings.LDAP_PASSWORD):
    """
    Establish ldap connection to RICE university CAS service
    """

    connection = ldap.initialize(host_name)
    connection.protocol_version = ldap.VERSION3
    connection.simple_bind_s(user, password)
        
    return connection


def get_cas_userdata(lookup_value, lookup_key="uid", connection=None,
                     user_base_dn='ou=People,dc=rice,dc=edu',
                     users_filter='(objectclass=person)(riceuserstatus=active)',
                     retrieve=[
                         'uid',
                         'givenName',
                         'sn',
                         'mail',
                         'eduPersonPrimaryAffiliation',
                         'riceClass',
                         'riceID',
                         'riceDOB',
                         'riceGender',
                         'riceOrg',
                         'riceCollege',
                         'riceCourse',
                         'riceDisplayOption']):
    """
    Lookup for user attributes
    """    
    if not connection:
        connection = create_cas_connection()

    query = '(&%s(%s=%s))' % (users_filter, lookup_key, lookup_value)

    try:
        ldap_results = connection.search_s(user_base_dn, ldap.SCOPE_SUBTREE, query, retrieve)
        if len(ldap_results) == 0:
            return None
        ldap_result = ldap_results[0][1]

        rice_user_data = {}
        rice_user_data['username'] = ldap_result.get('uid', [lookup_value if lookup_key =='uid' else ''])[0]
        rice_user_data['first_name'] = ldap_result.get('givenName', [''])[0]
        rice_user_data['last_name'] = ldap_result.get('sn', [''])[0]
        rice_user_data['email'] = ldap_result.get('mail', [''])[0]
        rice_user_data['affiliation'] = ldap_result.get('eduPersonPrimaryAffiliation', [''])[0]
        rice_user_data['classification'] = ldap_result.get('riceClass', [''])[0]
        rice_user_data['rice_id'] = ldap_result.get('riceID', [''])[0]
        rice_user_data['date_of_birth'] = ldap_result.get('riceDOB', [''])[0]
        rice_user_data['gender'] = ldap_result.get('riceGender', [''])[0]
        rice_user_data['rice_org'] = ldap_result.get('riceOrg', [''])[0]
        rice_user_data['college'] = ldap_result.get('riceCollege', [''])[0]
        rice_user_data['course'] = ldap_result.get('riceCourse', [''])[0]
        rice_user_data['display_option'] = ldap_result.get('riceDisplayOption', [''])[0]
        return rice_user_data
    except:
        return None


def get_or_create_django_user(rice_user_data):
    """
    Create django user from cas data or return existing one
    """
    username = rice_user_data['username']

    try:
        user = User.objects.get(username__iexact=username)
    except User.DoesNotExist:
        first_name = rice_user_data.get('first_name','')
        last_name = rice_user_data.get('last_name','')
        email = rice_user_data.get('email','')

        user = User.objects.create_user(username,email,None)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
    return user


def _verify_cas1(ticket, service):
    """Verifies CAS 1.0 authentication ticket.

    Returns username on success and None on failure.
    """

    params = {'ticket': ticket, 'service': service}
    url = (urljoin(settings.CAS_SERVER_URL, 'validate') + '?' +
           urlencode(params))
    page = urlopen(url)
    try:
        verified = page.readline().strip()
        if verified == 'yes':
            return page.readline().strip()
        else:
            return None
    finally:
        page.close()


def _verify_cas2(ticket, service):
    """Verifies CAS 2.0+ XML-based authentication ticket.

    Returns username on success and None on failure.
    """

    try:
        from xml.etree import ElementTree
    except ImportError:
        from elementtree import ElementTree

    params = {'ticket': ticket, 'service': service}
    url = (urljoin(settings.CAS_SERVER_URL, 'proxyValidate') + '?' +
           urlencode(params))
    page = urlopen(url)
    try:
        response = page.read()
        tree = ElementTree.fromstring(response)
        if tree[0].tag.endswith('authenticationSuccess'):
            return tree[0][0].text
        else:
            return None
    finally:
        page.close()


_PROTOCOLS = {'1': _verify_cas1, '2': _verify_cas2}

if settings.CAS_VERSION not in _PROTOCOLS:
    raise ValueError('Unsupported CAS_VERSION %r' % settings.CAS_VERSION)

_verify = _PROTOCOLS[settings.CAS_VERSION]


class CASBackend(object):
    """CAS authentication backend"""
    
    supports_object_permissions = False
    supports_anonymous_user = False
    supports_inactive_user = False

    def authenticate(self, ticket, service):
        """Verifies CAS ticket and gets or creates User object"""
        
        username = _verify(ticket, service)

        if not username:
            return None
        
        try:
            # Bind to LDAP
            connection = create_cas_connection()
            
            # Build our LDAP query
            rice_user_data = get_cas_userdata(username, connection=connection)
        except:
            rice_user_data = None
            
        if rice_user_data is None:
            rice_user_data = {'username':username}
            
        user = get_or_create_django_user(rice_user_data)
        cas_user_logged_in.send(sender=self,user=user,user_data=rice_user_data)

        return user

    def get_user(self, user_id):
        """Retrieve the user's entry in the User model if it exists"""

        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
