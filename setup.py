from distutils.core import setup

setup(name='django_cas',
	  version='2.6',
	  py_modules=['django_cas'],
	  )
