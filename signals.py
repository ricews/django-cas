from django.dispatch import Signal

cas_user_logged_in = Signal(providing_args=['user','userdata'])